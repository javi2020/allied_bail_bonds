var ajxLoc = 'https://panicmanagerupdate2.1solutionapps.com/';
// on page load check if panic is active
$(function () {
    panicActive();
});

// show contact 2 on panic form
$("#show-panic-2").on("click", function (e) {
    e.preventDefault();
    $('#panic-contact-2').removeClass('hidden');
    $('#show-panic-2').addClass('hidden');
});

// show contact 3 on panic form
$("#show-panic-3").on("click", function (e) {
    e.preventDefault();
    $('#panic-contact-3').removeClass('hidden');
    $('#show-panic-3').addClass('hidden');
});

// is panic active?
function panicActive() {
    var user = localStorage.getItem('user');
    if (user != null) {
        $("#panic-btn").attr("src", "panic_btn-armed.png");
        $(".panic-reset").removeClass('d-none');
        if (localStorage.getItem('panic')) {
            $('.panic-reset').html('<p align="center">Request Sent.</p>');
            localStorage.removeItem('panic');
        }
        return true;
    }
    return false;
}

function sendPanic() {
    console.log('sent');
    $('.panic-reset').html()

    $('#panic-btn').hide();
    $('.panic-reset').html('<p align="center">Sending...</p>');
    var loader = $('<img src="https://1solutionapps.com/images/loader.gif" width="90%" />').appendTo('.panic-reset');
    loader.css({ display: 'block', margin: '0 auto' });

    $.ajax({
        type: 'POST',
        url: 'https://onesolutionapps.com/applications/osaJS/GPS.php',
        success: function (data) {
            $('body').append(data);
            var gps = new GPS(function (formattedAddress, mapLink) {
                $('#panic-btn').hide();
                $('.panic-reset').html('<p align="center">Sending...</p>');
                var loader = $('<img src="https://1solutionapps.com/images/loader.gif" width="90%" />').appendTo('.panic-reset');
                loader.css({ display: 'block', margin: '0 auto' });
                var dataString = 'gpsAddress=' + formattedAddress + '&gpsMapLink=' + mapLink + '&userId=' + localStorage.getItem('user');
                $.ajax({
                    url: ajxLoc + "frontEnd/sendPanic",
                    data: dataString,
                    type: 'POST',
                    success: function (data) {
                        loader.remove();
                        localStorage.setItem('panic', 'sent');
                        return document.location.reload(true);
                    },
                    error: function (e) { console.log(e.responseText); alert('error sending panic to bailPanic.php') }
                });
            });
        },
        error: function (response) {
            $('#panic-btn').hide();
            $('.panic-reset').html('<p align="center">Sending...</p>');
            var loader = $('<img src="https://1solutionapps.com/images/loader.gif" width="90%" />').appendTo('.panic-reset');
            loader.css({ display: 'block', margin: '0 auto' });
            var dataString = 'action=sendPanic&email=' + _this.email + '&phone=' + _this.phone + '&company=' + _this.companyName + '&address=&mapLink=' + '&armedid=' + localStorage.getItem('user');
            $.ajax({
                url: ajxLoc + "bailPanic.php",
                data: dataString,
                type: 'POST',
                success: function (data) {
                    //alert(data);
                    loader.remove();
                    localStorage.setItem('panic', 'sent');
                    return document.location.reload(true);
                }
            });
        }
    });
}

$("#panic-form").submit(function (e) {
    e.preventDefault();
}).validate({
    rules: {
        name: {
            required: true,
            minlength: 2
        },
        number: {
            required: true,
            phoneUS: true
        },
        name1: {
            required: true,
            minlength: 2
        },
        number1: {
            required: true,
            phoneUS: true
        },
        rel1: {
            required: true,
            minlength: 2
        }
    },
    messages: {
        name: {
            required: "Please enter your name",
            minlength: "Your name must consist of at least 2 characters"
        },
        number: {
            required: "Please enter your phone number",
            phoneUS: "Your phone number appears invalid"
        },
        name1: {
            required: "Please enter a conact name",
            minlength: "Name must consist of at least 2 characters"
        },
        number1: {
            required: "Please enter a contact phone number",
            phoneUS: "Contact phone number appears invalid"
        },
        rel1: {
            required: "Please enter contact relationship",
            minlength: "Relationship must consist of at least 2 characters"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");

        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.next("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
    },
    submitHandler: function (form) {
        var companyId = $('#panic-script').attr('company-id');
        var dataString = $(form).serialize() + '&companyId=' + companyId + '&action=submit';

        $('.panic-button-text').html('Arming system...');
        var loader = $('<img src="https://1solutionapps.com/images/loader.gif" width="90%" style="margin: auto;" />').appendTo('.panic-button-text');

        //submit via ajax
        $.ajax({
            url: ajxLoc + "frontEnd/submitForm",
            data: dataString,
            type: 'POST',
            success: function (data) {
                $('.home').css('text-align', 'center').html('<p align="center">success...</p>');
                localStorage.setItem('user', data);
                loader.remove();
                return document.location.reload(true);
            },
            error: function (e) {
                $('.home').css('text-align', 'center').html('<p align="center">fail...</p>');
                console.log(e.responseText);
            }
        });

        return false;
    }
});

// clear panic on button click
$(".panic-reset").on("click", function (e) {
    clearPanic();
});

clearPanic = function () {
    localStorage.removeItem('user');
    return document.location.reload(true);
}

// phone validation rule
$.validator.addMethod("phoneUS", function (phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");
