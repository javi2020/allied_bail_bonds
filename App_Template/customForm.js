/*------------------------------------------------------------------------------
customForm.js
------------------------------------------------------------------------------*/

/**
 * function initForm(formName, apiEndpoint, returnPage)
 *
 * @param formName    | the name of the form that is passed to the email
 * @param apiEndpoint | the api enpoint to be passed to the email
 * @param returnPage  | the page that the app returns to after email is sent
 *
 * Initializes the custom form and passes through desired parameters.
 */
function initForm(formName, apiEndpoint, returnPage) {

    // initialize the loading modal
    initLoadingModal();

    /**
     * addEventListener 'click'
     *
     * Sets an onclick event for specific elements.
     */
    document.addEventListener('click', (event) => {
        const target = event.target;

        // click event for next page buttons
        if (target.matches('.next-btn')) {

            if (isValidated(target)) {

                getNextPage(target);
            }
        }
        // click event for submit button
        else if (target.matches('#submit-btn')) {

            if (isValidated(target)) {

                setTimeout(function () {

                    // send email with our parameters
                    sendEmail(formName, apiEndpoint, returnPage);
                }, 10);
            }
        }
    });
}

/**
 * function initLoadingModal()
 *
 * Creates a hidden loading modal at the top of the page.
 */
function initLoadingModal() {
    const modal = document.createElement('div');

    modal.innerHTML = `
        <section id="loading-modal-section" class="display-none">
            <div class="container">
                <img src="loading.gif">
                <br>
                <h1>
                    Uploading Your Information
                    <br>
                    This will only take a moment
                </h1>
            </div>
        </section>
    `;

    document.body.insertBefore(modal, document.body.firstChild);
}

/**
 * function isValidated()
 *
 * Checks if the inputs and selects are completed and returns false if not.
 * Displays a nice little error animation on false return.
 */
function isValidated(target) {
    const section = document.querySelector('#form-section');
    const form    = target.parentNode.querySelector('container');
    const inputs  = target.parentNode.querySelectorAll('input');
    const selects = target.parentNode.querySelectorAll('select');
    var   valid   = true;

    // for each text input, check if the required ones are complete
    inputs.forEach(input => {

        if (input.hasAttribute('required') && input.value === '') {

            section.className = 'invalid-form';

            setTimeout(() => {

                section.className = '';
            }, 750);

            valid = false;
        }
    });

    // for each select input, check if the required ones are complete
    selects.forEach(select => {

        if (select.hasAttribute('required') && select.value === '') {

            section.className = 'invalid-form';

            setTimeout(() => {

                section.className = '';
            }, 750);

            valid = false;
        }
    });

    return valid;
}

/**
 * function getFeatureData()
 *
 * Looks for additional features and adds that data to the data string that is
 * sent to the email API;
 */
function getFeatureData() {
    var dataString = '';

    if (document.querySelector('.sig-wrapper')) {
        var signatures = document.querySelectorAll('canvas');

        signatures.forEach((signature, i) => {
            const canvasName   = signature.getAttribute('name');
            const signatureURL = signature.toDataURL();

            dataString += '","'+ canvasName +'":"' + signatureURL;
            document.querySelector('.sig-wrapper').style.opacity = '0';
        });
    }

    return dataString;
}

/**
 * function initForm(formName, apiEndpoint, returnPage)
 *
 * @param formName    | the name of the form that is passed to the email
 * @param apiEndpoint | the api enpoint to be passed to the email
 * @param returnPage  | the page that the app returns to after email is sent
 *
 * Sends an email with the form information to the client.
 */
function sendEmail(formName, apiEndpoint, returnPage) {
    const loadElem    = document.querySelector('#loading-modal-section');
    const loadCont    = document.querySelector('#loading-modal-section .container');
    const inputs      = document.querySelectorAll('#form-section input');
    const selects     = document.querySelectorAll('#form-section select');
    var   clientField = [];
    var   clientData  = [];
    var   dataString  = '';

    loadElem.className = '';
    dataString        += 'FORMNAME":"' + formName;

    // for each text input type, store that information
    inputs.forEach(input => {
        if (input.className !== 'otherInput' && input.className !== 'upload-input' ) {

            clientField.push(input.getAttribute('name'));
            clientData.push(input.value);
        }
    });

    // for each select input type, store that information
    selects.forEach(select => {

        clientField.push(select.getAttribute('name'));
        clientData.push(select.value);
    });

    // for each input, add its value to the data string that is sent over ajax
    for (var i = 0; i < clientData.length; i++) {

        dataString += '","' + clientField[i] + '":"' + clientData[i];
    }

    // adds any additional data from features into the data string
    dataString += getFeatureData();

    /**
     * AJAX POST
     *
     * Sends the dataString data to our api endpoint and returns to the return
     * page on completion.
     */
    $.ajax({
        type:        'POST',
        url:         apiEndpoint,
        crossDomain: true,
        cache:       false,
        header:      'Access-Control-Allow-Origin: *',
        contentType: "application/json; charset=utf-8",
        dataType:    'json',
        data:        '{"' + dataString + '"}',
        complete:    () => {

            setTimeout(() => {
                location.href = returnPage;
            }, 4500);

            setTimeout(() => {
                loadCont.innerHTML = `<br><h1>Thank you for your submission!</h1>`;
            }, 3000);
        }
    });
}
