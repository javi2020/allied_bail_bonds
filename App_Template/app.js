// // carousel navigation
// $(".carousel-link").on("click", function (e) {
//     page = $(this).attr("data-page");
//     if (page === "ask") window.open("ask.html");
//     else openPage(page);
// });

// // change page by hash
// hash = window.location.hash.replace("#", "");
// if (hash !== "") {
//     openPage(hash);
// }

// // footer navigation
// $(".nav-link").on("click", function (e) {
//     page = $(this).attr("data-page");
//     openPage(page);
// });

// // page navigation
// $(".body-link").on("click", function (e) {
//     page = $(this).attr("data-page");
//     openPage(page);
// });

// function openPage(page) {
//     switch (page) {
//         case "forms":
//             openSection("forms", "forms");
//             break;
//         case "more":
//             openSection("more", "more");
//             break;
//         case "share":
//             openSection("share", "contact");
//             $("#share-content").load("share.html");
//             break;
//         case "about":
//             openSection("about", "more");
//             break;
//         case "ask-agent":
//             openSection("ask-agent", "more");
//             break;
//         case "useful-info":
//             openSection("useful-info", "more");
//             break;
//         case "bail-skippers":
//             openSection("bail-skippers", "more");
//             break;
//         case "check-in":
//             openSection("check-in", "home");
//             break;
//         case "jails":
//             openSection("jails", "more");
//             break;
//         case "faqs":
//             openSection("faqs", "useful-info");
//             break;
//         case "legal-terms":
//             openSection("legal-terms", "useful-info");
//             break;
//         case "your-rights":
//             openSection("your-rights", "useful-info");
//             break;
//         case "panic":
//             if (panicActive()) {
//                 sendPanic();
//             } else openSection("panic", "home");
//             break;
//         case "warrant":
//             openSection("warrant", "forms");
//             $("#warrant-content").load("warrantsearch.html");
//             break;
//         case "payment":
//             window.location = "https://fivestar-bailbonds.com/make-a-payment";
//             break;
//         case "bond":
//             window.location = "https://fivestar-bailbonds.com/post-a-bond-online"
//             break;
//         default:
//             panicActive();
//             openSection("home", "home");
//     }
// }

// function openSection(page, menu) {
//     resetPages();
//     $(".nav-button-" + menu).removeClass("d-none");
//     $(".nav-icon-" + menu).addClass("d-none");
//     $("#" + page).removeClass("hidden");
//     $('html, body').animate({ scrollTop: 0 }, 'fast');
// }

// function resetPages() {
//     $(".nav-icon").removeClass("d-none");
//     $(".nav-button").addClass("d-none");
//     $(".pages").addClass("hidden");
//     $('#panic-contact-2').addClass('hidden');
//     $('#panic-contact-3').addClass('hidden');
//     $('#show-panic-2').removeClass('hidden');
// }