## Main colors:

- ![#541614](https://via.placeholder.com/20/541614/000000?text=+) `#541614`

- ![#F2F2F2](https://via.placeholder.com/20/F2F2F2/000000?text=+) `#F2F2F2`

## Notifications:

- Bell:

  - ![#AA0202](https://via.placeholder.com/20/AA0202/000000?text=+) `#AA0202`

-  Bell-Background:  

    - ![#541614](https://via.placeholder.com/20/541614/000000?text=+) `#541614` 10% opacity

## Nav-Colors:

- Nav-background: 

    - ![#541614](https://via.placeholder.com/20/541614/000000?text=+) `#541614`

- Nav-Icon-unselected:

    - ![#FFFFFF](https://via.placeholder.com/20/FFFFFF/000000?text=+) `#FFFFFF`

- Nav-Icon-Selected:

    - ![#AA0202](https://via.placeholder.com/20/AA0202/000000?text=+) `#AA0202`



